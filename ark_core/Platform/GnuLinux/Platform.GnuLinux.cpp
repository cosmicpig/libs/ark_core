#include "ark_core/Platform/Discovery.hpp"
#if (ARK_CURRENT_OS == ARK_OS_GNU_LINUX)
// Header
#include "ark_core/Platform/PlatformCommon.hpp"
#include "ark_core/Platform/GnuLinux/Platform.GnuLinux.hpp"
// Arkadia
#include "ark_core/Platform/PlatformIncludes.hpp"
#include "ark_core/Error.hpp"
#include "ark_core/Path.hpp"

//
// Error Management
//
//------------------------------------------------------------------------------
i32
ark::Platform::GetPlatformLastError()
{
    return GetCLibLastError();
}

//------------------------------------------------------------------------------
ark::String
ark::Platform::GetPlatformLastErrorAsString(i32 const error_code)
{
    return GetCLibLastErrorAsString(error_code);
}

//------------------------------------------------------------------------------
ark::Error
ark::Platform::GetPlatformLastErrorAsArkError(i32 const error_code)
{
    return GetCLibLastErrorAsArkError(error_code);
}

//------------------------------------------------------------------------------
ark::ErrorCodes::Codes
ark::Platform::TranslatePlatformErrorToArk(i32 const err)
{
    return TranslateCLibErrorToArk(err);
}


//------------------------------------------------------------------------------
i32
ark::Platform::GetCLibLastError()
{
    return errno;
}

//------------------------------------------------------------------------------
ark::String
ark::Platform::GetCLibLastErrorAsString(i32 const error_code /* = INVALID_ERROR_CODE */)
{
    return strerror(error_code);
}

//------------------------------------------------------------------------------
ark::Error
ark::Platform::GetCLibLastErrorAsArkError(i32 const error_code /* = INVALID_ERROR_CODE */)
{
    i32 const error = (error_code != INVALID_ERROR_CODE) ? error_code : errno;
    ark::Error const ark_error = ark::Error(
        TranslatePlatformErrorToArk (error),
        GetPlatformLastErrorAsString(error)
    );
    return ark_error;
}

//------------------------------------------------------------------------------
ark::ErrorCodes::Codes
ark::Platform::TranslateCLibErrorToArk(i32 const err)
{
    // @todo(stdmatt): Implement... April 01, 21
    switch(err) {
        case ENOMEM:
            return ark::ErrorCodes::MEMORY_NOT_ENOUGH_SPACE;
            // File / Path
        case EACCES:
        case EPERM:
            return ark::ErrorCodes::FILEPATH_INVALID_ACCESS;
        case EINVAL:
            return ark::ErrorCodes::FILEPATH_INVALID_FILENAME;
        case ENOTDIR:
        case ELOOP:
        case EFAULT: // @TODO(stdmatt): EFAULT can happen in other conditions?
            return ark::ErrorCodes::FILEPATH_INVALID_PATH;
        case EEXIST:
            return ark::ErrorCodes::FILEPATH_ALREADY_EXISTS;
        case ENAMETOOLONG:
            return ark::ErrorCodes::FILEPATH_TOO_LONG;
        case ENOSPC:
        case EMLINK:
            return ark::ErrorCodes::FILEPATH_NOT_ENOUGH_SPACE;
        case EROFS:
            return ark::ErrorCodes::FILEPATH_CANNOT_WRITE;
    }

    return ark::ErrorCodes::UNKNOWN_ERROR;
}

//
//
//
//------------------------------------------------------------------------------
ark::String
ark::Platform::GetProgramName(bool const remove_extension /* = true */)
{
    auto program_name = program_invocation_short_name;
    return program_name;
}

//------------------------------------------------------------------------------
ark::String
ark::Platform::GetProgramDir()
{
    ark::String const program_dir = ark::PathUtils::Dirname(program_invocation_name);
    return program_dir;
}


#endif // (ARK_CURRENT_OS == ARK_OS_GNU_LINUX)
