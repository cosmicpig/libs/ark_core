﻿#include "ark_core/Platform/Discovery.hpp"
#if (ARK_OS_IS_WINDOWS)
// Header
#include "ark_core/Platform/PlatformCommon.hpp"
#include "ark_core/Platform/Win32/Platform.Win32.hpp"
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Path/PathUtils.hpp"
#include "ark_core/IO/IOCommon.hpp"

//
// Error Management
//
//------------------------------------------------------------------------------
i32
ark::Platform::GetPlatformLastError()
{
    return GetLastError();
}

//------------------------------------------------------------------------------
ark::String
ark::Platform::GetPlatformLastErrorAsString(i32 const error_code)
{
    DWORD const error = (error_code != INVALID_ERROR_CODE) ? error_code : GetLastError();
    if(!error) {
        return ark::String::Empty();
    }

    LPVOID msg_buffer = nullptr;
    DWORD const buffer_len = FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        nullptr,
        error,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        Cast<LPTSTR>(&msg_buffer),
        0,
        nullptr
    );

    if(!buffer_len) {
        return ark::String::Empty();
    }

    ark::String result = Cast<LPCSTR>(msg_buffer);
    LocalFree(msg_buffer);

    return result;
}

//------------------------------------------------------------------------------
ark::Error
ark::Platform::GetPlatformLastErrorAsArkError(i32 const error_code)
{
    DWORD      const error     = (error_code != INVALID_ERROR_CODE) ? error_code : GetLastError();
    ark::Error const ark_error = ark::Error(
        TranslatePlatformErrorToArk (error),
        GetPlatformLastErrorAsString(error)
    );
    return ark_error;
}

//------------------------------------------------------------------------------
ark::ErrorCodes::Codes
ark::Platform::TranslatePlatformErrorToArk(i32 const err)
{
    // @todo(stdmatt): Implement... Jan 30, 21
    switch(err) {
        case ERROR_ACCESS_DENIED: return ark::ErrorCodes::ACCESS_DENIED;
    }

    return ark::ErrorCodes::UNKNOWN_ERROR;
}


//------------------------------------------------------------------------------
i32
ark::Platform::GetCLibLastError()
{
    return errno;
}

//------------------------------------------------------------------------------
ark::String
ark::Platform::GetCLibLastErrorAsString(i32 const error_code /* = INVALID_ERROR_CODE */)
{
    return ::strerror(error_code);
}

//------------------------------------------------------------------------------
ark::Error
ark::Platform::GetCLibLastErrorAsArkError(i32 const error_code /* = INVALID_ERROR_CODE */)
{
    DWORD      const error     = (error_code != INVALID_ERROR_CODE) ? error_code : errno;
    ark::Error const ark_error = ark::Error(
        TranslatePlatformErrorToArk (error),
        GetPlatformLastErrorAsString(error)
    );
    return ark_error;
}

//------------------------------------------------------------------------------
ark::ErrorCodes::Codes
ark::Platform::TranslateCLibErrorToArk(i32 const err)
{
    // @todo(stdmatt): Implement... April 01, 21
    switch(err) {
        case ENOMEM:
            return ark::ErrorCodes::MEMORY_NOT_ENOUGH_SPACE;
        // File / Path
        case EACCES:
        case EPERM:
            return ark::ErrorCodes::FILEPATH_INVALID_ACCESS;
        case EINVAL:
            return ark::ErrorCodes::FILEPATH_INVALID_FILENAME;
        case ENOTDIR:
        case ELOOP:
        case EFAULT: // @TODO(stdmatt): EFAULT can happen in other conditions?
            return ark::ErrorCodes::FILEPATH_INVALID_PATH;
        case EEXIST:
            return ark::ErrorCodes::FILEPATH_ALREADY_EXISTS;
        case ENAMETOOLONG:
             return ark::ErrorCodes::FILEPATH_TOO_LONG;
        case ENOSPC:
        case EMLINK:
            return ark::ErrorCodes::FILEPATH_NOT_ENOUGH_SPACE;
        case EROFS:
            return ark::ErrorCodes::FILEPATH_CANNOT_WRITE;
    }

    return ark::ErrorCodes::UNKNOWN_ERROR;
}


//
//
//
//------------------------------------------------------------------------------
ark::String
ark::Platform::GetProgramName(bool const remove_extension /* = true */)
{
    HMODULE const us = GetModuleHandle(nullptr);

    ARK_STRICT_CONSTEXPR u32 FILENAME_MAX_SIZE = 1024;
    char filename_buffer[FILENAME_MAX_SIZE];

    GetModuleFileName(us, filename_buffer, FILENAME_MAX_SIZE);
    String filename = String(filename_buffer);
    filename = PathUtils::Basename(filename);

    if(remove_extension) {
        filename = PathUtils::RemoveExtension(filename);
    }
    return filename;
}

//------------------------------------------------------------------------------
ark::String
ark::Platform::GetProgramDir()
{
    HMODULE const us = GetModuleHandle(nullptr);

    ARK_STRICT_CONSTEXPR u32 FILENAME_MAX_SIZE = 1024;
    char filename_buffer[FILENAME_MAX_SIZE];

    GetModuleFileName(us, filename_buffer, FILENAME_MAX_SIZE);
    String dirname = String(filename_buffer);
    dirname = PathUtils::Canonize(dirname);
    dirname = PathUtils::Dirname (dirname);

    return dirname;
}


//
// Error Management
//
//------------------------------------------------------------------------------
// Create a string with last error message


//
// Memory Management
//
//------------------------------------------------------------------------------
void*
ark::Win32::AllocMemory(size_t const size)
{
    void *memory = VirtualAlloc(0, size, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
    ARK_ASSERT(memory != nullptr, "Failed to allocated memory - Size ({})", size);
    return memory;
}

//------------------------------------------------------------------------------
void
ark::Win32::ReleaseMemory(void *&memory)
{
    if(!memory) {
        return;
    }

    VirtualFree(memory, 0, MEM_RELEASE);
    memory = nullptr;
}

//
// Process Management
//
//------------------------------------------------------------------------------
ark::String
ark::Win32::FindExecutableNameFromProcessId(ProcessId_t const process_id)
{
    HANDLE const process_handle = OpenProcess(
        PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
        FALSE,
        process_id
    );

    if(!process_handle) {
        return String::Empty();
    }

    // Get a list of all the modules in this process.
    String  result_str;
    HMODULE module_handles[1024]; // @TODO(stdmatt): Bulletproof - Feb 10, 2021
    DWORD   cb_needed;
    if(EnumProcessModulesEx(process_handle, module_handles, sizeof(module_handles), &cb_needed, LIST_MODULES_ALL)) {
        for(u32 i = 0, module_handles_count = (cb_needed / sizeof(HMODULE));
            i < module_handles_count;
            ++i)
        {
            // Get the full path to the module's file.
            char module_name[MAX_PATH] = {};
            if(GetModuleFileNameEx(process_handle, module_handles[i], module_name, sizeof(module_name) / sizeof(char))) {
                // Now we need to make sure that we are grabbing just the .exe not all the .dlls and stuff
                if(strstr(module_name, ".exe") != nullptr) {
                    result_str = module_name;
                    goto _Exit;
                }
            }
        }
    }
_Exit:
    CloseHandle(process_handle);
    return result_str;
}


//
// Time Functions
//
//------------------------------------------------------------------------------

constexpr u64 SECONDS_BETWEEN_FILETIME_AND_POSIX       =11644473600; // 11643609600; // 11644473600;
constexpr u64 MILLISECONDS_BETWEEN_FILETIME_AND_POSIX  = SECONDS_BETWEEN_FILETIME_AND_POSIX * 1000;

u32
ark::Win32::FileTime_to_POSIX(FILETIME const file_time)
{
    // Thanks to: http://www.frenk.com/2009/12/convert-filetime-to-unix-timestamp/
    // takes the last modified date
    ULARGE_INTEGER date = {};
    date.HighPart = file_time.dwHighDateTime;
    date.LowPart  = file_time.dwLowDateTime;
    // 100-nanoseconds = milliseconds * 10000
    // removes the diff between 1970 and 1601
    date.QuadPart -= (MILLISECONDS_BETWEEN_FILETIME_AND_POSIX * 10000);
    // converts back from 100-nanoseconds to seconds
    u32 const result = Cast<u32>(date.QuadPart / 10000000);
    return result;
}

//------------------------------------------------------------------------------
FILETIME
ark::Win32::POSIX_to_FileTime(ark::time_t const posix_time)
{
    // 100ns == 0.0000001s
    // 1s == 1000000000ns
    // Posix Time   : Number of seconds since 00:00 1/1/1970
    // Windows Time : Number of 100-nanosecond intervals since 00:00 1/1/1601

    u64 const total_seconds_since_win32_epoch = (SECONDS_BETWEEN_FILETIME_AND_POSIX + posix_time);
    u64 const total_nanoseconds_elapsed       = (total_seconds_since_win32_epoch * 1000000000);
    u64 const total_100_nanoseconds_interval  = (total_nanoseconds_elapsed / 100);

    ULARGE_INTEGER date = {};
    date.QuadPart = total_100_nanoseconds_interval;

    FILETIME file_time = {};
    file_time.dwHighDateTime = date.HighPart;
    file_time.dwLowDateTime  = date.LowPart;

    return file_time;
}

//
// File Functions
//
//------------------------------------------------------------------------------
HANDLE
ark::Win32::GetFileHandle(ark::String const &path)
{
    HANDLE const handle = CreateFileA(
        path.CStr(),
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
        nullptr,
        OPEN_EXISTING,
        0,
        nullptr
    );

    return handle;
}


//
// Misc Functions
//
//------------------------------------------------------------------------------
HICON
ark::Win32::LoadIconFromFile(HINSTANCE const instance, ark::String const &filename)
{
    UINT const  icon_flags  = LR_LOADFROMFILE | LR_DEFAULTSIZE;
    HICON const icon_handle = Cast<HICON>(LoadImage(instance, filename.CStr(), IMAGE_ICON, 0, 0, icon_flags));
    return icon_handle;
}

//------------------------------------------------------------------------------
bool
ark::Win32::IsKeyboardWM(u32 const msg_type)
{
    switch(msg_type) {
        case WM_KEYDOWN    :
        case WM_KEYUP      :
        case WM_SYSKEYUP   :
        case WM_SYSKEYDOWN : return true;
    }

    return false;
}

//------------------------------------------------------------------------------
constexpr u32 _XXX_ARK_VK_BACK_TICK = VK_OEM_3;
bool is_alt_down     = false;
bool is_control_down = false;
bool is_shift_down   = false;

ark::KeyboardEvent
ark::Win32::TranslateWin32KeyToKeyboardEvent(
    u32 const msg_type,
    u32 const virtual_key_code,
    u32 const flags)
{
    ark::KeyboardEvent data = {};
    if(virtual_key_code == VK_LMENU) {
        is_alt_down = (msg_type == WM_SYSKEYDOWN);
    }
    if(virtual_key_code == VK_LCONTROL) {
        is_control_down = (msg_type == WM_KEYDOWN) || (msg_type == WM_SYSKEYDOWN);;
    }
    if(virtual_key_code == VK_LSHIFT) {
        is_shift_down = (msg_type == WM_KEYDOWN) || (msg_type == WM_SYSKEYDOWN);
    }

    data.is_down         = (msg_type == WM_SYSKEYDOWN || msg_type == WM_KEYDOWN);
    data.is_alt_down     = is_alt_down;
    data.is_control_down = is_control_down;
    data.is_shift_down   = is_shift_down;

    // @todo(stdmatt): We need to find a better way to do that....
    data.keycode_value = 0;
    switch(virtual_key_code) {
        case VK_UP    : data.keycode = ark::KeyCodes::Up    ; break;
        case VK_DOWN  : data.keycode = ark::KeyCodes::Down  ; break;
        case VK_LEFT  : data.keycode = ark::KeyCodes::Left  ; break;
        case VK_RIGHT : data.keycode = ark::KeyCodes::Right ; break;

        case VK_ESCAPE : data.keycode = ark::KeyCodes::Esc    ; break;
        case VK_RETURN : data.keycode = ark::KeyCodes::Return ; break;

        // @TODO(stdmatt): Handle other cases - Fev 01, 2021.
        case _XXX_ARK_VK_BACK_TICK: data.keycode = ark::KeyCodes::BackTick; break;
    }

    // ark::PrintLn("{}", ark::ToString(data));
    return data;
}


//------------------------------------------------------------------------------
ark::String
ark::Win32::GetClassName(HWND const handle)
{
    // @todo(stdmatt): Find a way to not have this buffer all the time... Feb 19, 21
    constexpr u32 CLASS_NAME_MAX_BUFFER_SIZE = 1023;
    char buffer[CLASS_NAME_MAX_BUFFER_SIZE] = {};

    i32 const result =::GetClassNameA(handle, buffer, CLASS_NAME_MAX_BUFFER_SIZE);
    if(result == 0) {
        return ark::String::Empty();
    }
    return buffer;
}

//
// Window Functions
//
//------------------------------------------------------------------------------
void
ark::Win32::SetWindowWidth(HWND const window_handle, u32 const value)
{
    RECT rect;
    GetClientRect(window_handle, &rect);
    SetWindowPos(window_handle, nullptr, 0, 0, value, rect.bottom - rect.top, SWP_NOMOVE);
}

//------------------------------------------------------------------------------
void
ark::Win32::SetWindowHeight(HWND const window_handle, u32 const value)
{
    RECT rect;
    GetClientRect(window_handle, &rect);
    SetWindowPos(window_handle, nullptr, 0, 0, rect.right - rect.left, value, SWP_NOMOVE);
}

#endif // (ARK_OS_IS_WINDOWS)
