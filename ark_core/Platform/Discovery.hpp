#pragma once
// std
#include "stdint.h"

//
// Assertions
//
//------------------------------------------------------------------------------
#define ARK_DISABLE_ASSERTIONS 0
#define ARK_ENABLE_ASSERTIONS  1

//------------------------------------------------------------------------------
#define ARK_ASSERTION_MODE ARK_ENABLE_ASSERTIONS // @todo(stdmatt): This should come from the build system - Jan 06, 2021
#if (ARK_ASSERTION_MODE == ARK_ENABLE_ASSERTIONS)
    #define ARK_ASSERTION_IS_ENABLED  true
#else
    #define ARK_ASSERTION_IS_ENABLED false
#endif // (ARK_ASSERTION_MODE == ARK_ENABLE_ASSERTIONS)

#define ARK_ASSERTION_IS_DISABLED (!(ARK_ASSERT_IS_ENABLED))

//
// Build Mode
//
//------------------------------------------------------------------------------
#define ARK_BUILD_MODE_DEBUG   1
#define ARK_BUILD_MODE_RELEASE 2

//------------------------------------------------------------------------------
#define ARK_CURRENT_BUILD_MODE ARK_BUILD_MODE_DEBUG
#if (ARK_CURRENT_BUILD_MODE == ARK_BUILD_MODE_DEBUG)
    #define ARK_IS_DEBUG_BUILD   true
    #define ARK_IS_RELEASE_BUILD false
#else
    #define ARK_IS_DEBUG_BUILD   false
    #define ARK_IS_RELEASE_BUILD true
#endif // (ARK_ASSERTION_MODE == ARK_ENABLE_ASSERTIONS)

//
// OS
//
//------------------------------------------------------------------------------
#define ARK_OS_INVALID    0
#define ARK_OS_WINDOWS    1
#define ARK_OS_DOS        2
#define ARK_OS_GNU_LINUX  3
#define ARK_OS_MAC        4

//------------------------------------------------------------------------------
// Win32
#if defined( _WIN32 )
    #define ARK_CURRENT_OS    ARK_OS_WINDOWS
    #define ARK_OS_IS_WINDOWS true
// GNU/Linux
#elif defined ( __gnu_linux__ ) ||  \
      defined ( __linux__     )
    #define ARK_CURRENT_OS      ARK_OS_GNU_LINUX
    #define ARK_OS_IS_GNU_LINUX true
// GNU/Linux
// @todo(stdmatt): Check all the defines possible...
//      i386-pc-msdosdjgpp-g++ -dM -E - < /dev/null
//      thanks leiradella - Jan 30, 21
#elif defined (__MSDOS__)
    #define ARK_CURRENT_OS  ARK_OS_DOS
    #define ARK_OS_IS_DOS   true
// Not supported OS
#else
    #error "OS not supported...";
#endif // Windows


//
// Compiler
//
//------------------------------------------------------------------------------
#define ARK_COMPILER_INVALID 0
#define ARK_COMPILER_GCC     1
#define ARK_COMPILER_CLANG   2
#define ARK_COMPILER_MSVC    3

//------------------------------------------------------------------------------
#if defined(__clang__)
    #define ARK_CURRENT_COMPILER  ARK_COMPILER_CLANG
    #define ARK_COMPILER_IS_CLANG true
#elif defined(__GNUC__)
    #define ARK_CURRENT_COMPILER ARK_COMPILER_GCC
    #define ARK_COMPILER_IS_GCC  true
#elif defined(_MSC_VER)
    #define ARK_CURRENT_COMPILER ARK_COMPILER_MSVC
    #define ARK_COMPILER_IS_MSVC true
#else
    #error "Compiler not supported...";
#endif


//
// Arch
//
//------------------------------------------------------------------------------
#define ARK_ARCH_32_BIT 1
#define ARK_ARCH_64_BIT 2

#if (INTPTR_MAX == INT64_MAX)
    #define ARK_CURRENT_ARCH   ARK_ARCH_64_BIT
    #define ARK_ARCH_IS_64_BIT true
#elif (INTPTR_MAX == INT32_MAX)
    #define ARK_CURRENT_ARCH   ARK_ARCH_32_BIT
    #define ARK_ARCH_IS_32_BIT true
#else
    #error Unknown pointer size or missing size macros!
#endif



#if ARK_EXCEPTIONS_IS_ENABLED && ARK_EXCEPTION_CATCH_ALL_IN_MAIN
#define ark_exception_catch_all_all_in_main(_code_) \
    try { _code_ } catch(std::execption const &ex) { };
#else
#define ARK_EXCEPTION_CATCH_ALL_ALL_IN_MAIN(_code_) \
    _code_
#endif // ARK_EXCEPTIONS_IS_ENABLED && ARK_EXCEPTION_CATCH_ALL_IN_MAIN
