﻿#pragma once
#include "ark_core/Platform/Discovery.hpp"
#if (ARK_OS_IS_DOS)
// DOS
#include <conio.h>
#include <dpmi.h>
#include <go32.h>
#include <pc.h>
#include <bios.h>
#include <sys/movedata.h>
#include <sys/farptr.h>
#include <sys/nearptr.h>
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Debug/Assert.hpp"

namespace ark { namespace DOS {

enum GraphicsMode {
    VGA_256_COLOR_MODE = 0x13,
};

//
// Graphics
//
bool IsGraphicsInit  ();
void GraphicsInit    (GraphicsMode const graphics_mode);
void GraphicsShutdown();

} // namespace DOS
} // namespace ark

#endif // (ARK_OS_IS_DOS)
