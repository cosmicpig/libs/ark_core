﻿#pragma once

//----------------------------------------------------------------------------//
// Fake Keywords                                                              //
//----------------------------------------------------------------------------//
#define ark_local_persist     static
#define ark_internal_function static
#define ark_global_var        static
