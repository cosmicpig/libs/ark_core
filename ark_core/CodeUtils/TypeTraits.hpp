﻿#pragma once
// std
#include <iterator>
#include <type_traits>

namespace ark { namespace TypeTraits {

//--------------------------------------------------------------------------
template<typename InputIterator>
using RequireInputIterator = typename
    std::enable_if<
        std::is_convertible<
            typename std::iterator_traits<InputIterator>::iterator_category,
            std::input_iterator_tag
        >::value>::type;
} // namespace TypeTraits
} // namespace ark
