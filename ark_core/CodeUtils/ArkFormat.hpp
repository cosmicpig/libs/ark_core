﻿#pragma once

// fmt
#include "fmt/format.h"

#define ARK_FMT_DEFINE_SIMPLE_FORMATTER(_class_, _str_, ...)             \
    template <>                                                          \
    struct fmt::formatter<_class_> {                                     \
        auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) { \
            return ctx.end();                                            \
        }                                                                \
        template <typename FormatContext>                                \
        auto format(_class_ const &var, FormatContext& ctx) {            \
            return format_to(ctx.out(), _str_, ##__VA_ARGS__);             \
        }                                                                \
    };
