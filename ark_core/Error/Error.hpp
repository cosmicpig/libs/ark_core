﻿#pragma once
//
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Containers/String.hpp"
#include "ark_core/Platform//PlatformIncludes.hpp"
namespace ark {

//
// All the error codes of ark_core
//
namespace ErrorCodes {
enum Codes {
    UNKNOWN_ERROR = -1,
    NO_ERROR      =  0,
    SUCCESS       =  NO_ERROR,

    // Memory
    MEMORY_NOT_ENOUGH_SPACE,      // GNU: ENOMEM,

    // File / Path
    FILEPATH_INVALID_ACCESS,      // GNU: EACCES | EPERM
    FILEPATH_INVALID_FILENAME,    // GNU: EINVAL
    FILEPATH_INVALID_PATH,        // GNU: ENOTDIR | ELOOP | EFAULT
    FILEPATH_NOT_FOUND,
    FILEPATH_ALREADY_EXISTS,      // GNU: EEXIST
    FILEPATH_TOO_LONG,            // GNU: ENAMETOOLONG
    FILEPATH_NOT_ENOUGH_SPACE,    // GNU: EQUOT | ENOSPC | EMLINK
    FILEPATH_CANNOT_WRITE,        // GNU: EROFS

    //
    ACCESS_DENIED // GNU: ??? -  Win32 : ERROR_ACCESS_DENIED
};
} // namespace ErrorCodes


//
//
//
struct Error {
    i32    error_code;
    String error_msg;

    ARK_FORCE_INLINE
    Error(i32 const error_code = ErrorCodes::NO_ERROR, String const &error_msg = String::Empty())
        : error_code(error_code)
        , error_msg (error_msg)
    {
        // Empty...
    }
}; // struct Error

} // namespace ark
