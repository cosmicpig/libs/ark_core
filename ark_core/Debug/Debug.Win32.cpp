﻿#include "ark_core/Platform/Discovery.hpp"
#if (ARK_OS_IS_WINDOWS)
// Header
#include "ark_core/Debug/Debug.hpp"
// Arkadia
#include "ark_core/Platform/Win32/Platform.Win32.hpp"

namespace ark {

//
// Public Functions.
//
//------------------------------------------------------------------------------
bool
DebugUtils::IsDebuggerPresent()
{
    return ::IsDebuggerPresent();
}

//------------------------------------------------------------------------------
void
DebugUtils::BreakInDebugger()
{
    ::DebugBreak();
}

} // namespace ark
#endif // (ARK_OS_IS_WINDOWS)
