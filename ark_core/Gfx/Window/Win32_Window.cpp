// Header
#include "ark_core/Gfx/Window/Win32_Window.hpp"
#if ARK_OS_IS_WINDOWS
// Arkadia
#include "ark_core/Memory.hpp"
#include "Platform/Win32/Platform.Win32.hpp"

namespace ark { namespace Gfx {

// @todo(stdmatt): For all Getters: Check if is better to cache those values? May 1, 21
// @todo(stdmatt): For all x11 calls, check the return values!! May 1, 21
Win32_Window *__hack_global_window = nullptr;

//
// Windows Loop
//
//------------------------------------------------------------------------------
LRESULT CALLBACK
Win32_WindowProcedure(
    HWND   const window_handle,
    UINT   const msg_type,
    WPARAM const w_param,
    LPARAM const l_param)
{
    Win32_Window *window = __hack_global_window;

    //
    // Keyboard Events
    if(ark::Win32::IsKeyboardWM(msg_type)) {
        if(window->_curr_event_weak_ptr) {
            window->_curr_event_weak_ptr->key_data = ark::Win32::TranslateWin32KeyToKeyboardEvent(
                msg_type,
                Cast<u32>( w_param),
                Cast<u32>(l_param)
            );
            window->_curr_event_weak_ptr->type = (window->_curr_event_weak_ptr->key_data.is_down)
                ? WindowEvent::Type::KeyDown
                : WindowEvent::Type::KeyUp;
        }
    }
    //
    // Other Events
    else {
        switch(msg_type) {
            //----------------------------------------------------------------------
            case WM_SIZE  : { window->OnWindowResize(); } break;
            case WM_PAINT : { window->OnWindowPaint (); } break;
            case WM_CLOSE : { window->OnWindowClose (); } break;

            //----------------------------------------------------------------------
            default : {
                if(msg_type > WM_USER                        &&
                   window->_dispatch_underlying_system_event &&
                   window->_curr_event_weak_ptr != nullptr)
                {
                    window->_curr_event_weak_ptr->type = WindowEvent::Type::UnderlyingSystem;
                    auto &ev = window->_curr_event_weak_ptr->underlying_system_data.data;

                    ev.window_handle = window_handle;
                    ev.msg_type      = msg_type;
                    ev.w_param       = w_param;
                    ev.l_param       = l_param;
                }
            }
        }
    }

    return DefWindowProc(window_handle, msg_type, w_param, l_param);
}

//
// Factory
//
//------------------------------------------------------------------------------
Window*
Window::Create(CreateOptions const &create_options)
{
    ARK_ASSERT_NULL(__hack_global_window);
    __hack_global_window = new Win32_Window();
    __hack_global_window->InitWithOptions(create_options);

    return __hack_global_window;
}

//------------------------------------------------------------------------------
void Window::Destroy(Window *&window)
{
    if(window) {
        Cast<Win32_Window*>(window)->DestroyInternal();
        ARK_SAFE_DELETE(window);
    }
}

//------------------------------------------------------------------------------
Window::CreateOptions
Window::GetDefaultCreateOptions()
{
    return {};
}


//
// Window Interface
//
//------------------------------------------------------------------------------
void *
Win32_Window::GetNativeHandle() const
{
    return Cast<void*>(this); // @todo(stdmatt): Which would be the native handle for X11? May 1, 21
}

//------------------------------------------------------------------------------
u32
Win32_Window::GetWidth() const
{
    return GetClientRect().h;
}

//------------------------------------------------------------------------------
u32
Win32_Window::GetHeight() const
{
    return GetClientRect().h;
}

//------------------------------------------------------------------------------
Rect_i32
Win32_Window::GetClientRect() const
{
    RECT r;
    ::GetClientRect(_window_handle, &r);
    return ark::Rect_i32(r.left, r.top, r.right - r.left, r.bottom - r.top);
}

//------------------------------------------------------------------------------
Vec2_i32
Win32_Window::GetClientSize() const
{
    auto const r = GetClientRect();
    return Vec2_i32(r.w, r.h);
}


//------------------------------------------------------------------------------
String
Win32_Window::GetTitle() const
{
    constexpr u32 BUFFER_SIZE = 255;
    char buffer[255];
    GetWindowText(_window_handle, buffer, BUFFER_SIZE);

    return buffer;
}

//------------------------------------------------------------------------------
bool
Win32_Window::WantsToClose() const
{
    return _wants_to_close;
}

//------------------------------------------------------------------------------
Vec2_i32
Win32_Window::GetMousePosition() const
{
    POINT p;
    if(!GetCursorPos(&p)) {
        // @todo(stdamtt): Handle errors - Mar 09, 21
        if (!ScreenToClient(_window_handle, &p)) {

        }
    }
    return ark::Vec2_i32(p.x, p.y);
}

//------------------------------------------------------------------------------
void
Win32_Window::SetTitle(String const &str)
{
    SetWindowText(_window_handle, str.CStr());
}

//------------------------------------------------------------------------------
void
Win32_Window::SetSize(u32 const width, u32 const height)
{
    ark::Win32::SetWindowHeight(_window_handle, width);
    ark::Win32::SetWindowWidth (_window_handle, height);
}

//------------------------------------------------------------------------------
void
Win32_Window::Show()
{
    ShowWindow(_window_handle, SW_SHOW);
}

//------------------------------------------------------------------------------
void
Win32_Window::Hide()
{
    ShowWindow(_window_handle, SW_HIDE);
}

//------------------------------------------------------------------------------
bool
Win32_Window::HandleEvents(WindowEvent *event, bool const dispatch_underlying_system_event /* = false */)
{
    ark_local_persist bool __hack_for_first_time = true;

    event->type = WindowEvent::Type::Invalid;
    _curr_event_weak_ptr = event;
    if(__hack_for_first_time) {
        __hack_for_first_time = false;
        OnWindowCreate();
        return true;
    }

    _dispatch_underlying_system_event = dispatch_underlying_system_event;
    MSG msg;
    if(PeekMessage(&msg, _window_handle, 0, 0, PM_REMOVE) > 0) {
        TranslateMessage(&msg);
        DispatchMessage (&msg);
        return true;
    }

    return false;
}

//------------------------------------------------------------------------------
bool
Win32_Window::HandleEventsBlocking(WindowEvent *event, bool const dispatch_underlying_system_event /* = false */)
{
    ark_local_persist bool __hack_for_first_time = true;

    // @todo(stdmatt): Same code from the HandleEvents but without the PeekMessage, refactor it - April 02, 2021
    event->type = WindowEvent::Type::Invalid;
    _curr_event_weak_ptr = event;
    if(__hack_for_first_time) {
        __hack_for_first_time = false;
        OnWindowCreate();
        return true;
    }

    _dispatch_underlying_system_event = dispatch_underlying_system_event;

    MSG msg;
    if(GetMessage(&msg, _window_handle, 0, 0) > 0) {
        TranslateMessage(&msg);
        DispatchMessage (&msg);
        return true;
    }

    return false;
}

//------------------------------------------------------------------------------
void
Win32_Window::CreateScreenBuffer(u32 const width, u32 const height, u32 const bpp)
{
    //
    // First time around.. So create the offscreen buffer.
    if(!_screen_buffer) {
        _screen_buffer = ark::Win32::AllocMemory<Win32_OffscreenBuffer>(sizeof(Win32_OffscreenBuffer));
        ark::ResetMemory(_screen_buffer);
    }

    //
    // Check if the total area is bigger, if not we can just reuse the
    // current buffer but with different width / area
    // @todo(stdmatt): Check how to free the memory if needded - May 2, 21
    u32 const new_size = ARK_AREA(width, height) * bpp;
    u32 const old_size = ARK_AREA(_screen_buffer->width, _screen_buffer->height) * _screen_buffer->bytes_per_pixel;
    if(new_size > old_size) {
        ark::Win32::ReleaseMemory(_screen_buffer->memory);
        _screen_buffer->memory = ark::Win32::AllocMemory<u8>(new_size);
    }

    _screen_buffer->width           = width;
    _screen_buffer->height          = height;
    _screen_buffer->pitch           = bpp * width;
    _screen_buffer->bytes_per_pixel = bpp;

    //
    // Handle the Bitmap info...
    //
    BITMAPINFO &bi = _screen_buffer->bitmap_info;
    ark::ResetMemory(&bi);

    bi.bmiHeader.biSize        = sizeof(bi.bmiHeader);
    bi.bmiHeader.biWidth       = +_screen_buffer->width;
    bi.bmiHeader.biHeight      = -Cast<i32>(_screen_buffer->height);
    bi.bmiHeader.biPlanes      = 1;
    bi.bmiHeader.biBitCount    = _screen_buffer->bytes_per_pixel * 8;
    bi.bmiHeader.biCompression = BI_RGB;
}

//------------------------------------------------------------------------------
OffscreenBuffer*
Win32_Window::GetCurrentScreenBuffer()
{
    return _screen_buffer;
}

//------------------------------------------------------------------------------
void
Win32_Window::FlushBuffers()
{
    HDC const hdc = GetDC(_window_handle);
    FlushBufferInternal(hdc);
    ReleaseDC( _window_handle, hdc);
}

//
// Helper Methods
//
//------------------------------------------------------------------------------
void
Win32_Window::InitWithOptions(Window::CreateOptions const &options)
{
    HINSTANCE const instance = GetModuleHandle(nullptr); // @todo(stdmatt): Check if this is correct... Jan 29, 21

    //
    // Register Window
    //
    // @todo(stdmatt): Need to rethink how to create the class name...
    ark_local_persist char const * const CLASS_NAME = "_ark_Win32_Doom_Fire";
    WNDCLASS wc = {};

    wc.lpfnWndProc   = Win32_WindowProcedure;
    wc.hInstance     = instance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    //
    // Create the window.
    //
    RECT r; r.top = 0; r.left = 0; r.right = options.width; r.bottom = options.height;
    AdjustWindowRect(&r, WS_OVERLAPPEDWINDOW, false);
    _window_handle = CreateWindowEx(
        0,
        CLASS_NAME,
        options.caption.CStr(),
        WS_OVERLAPPEDWINDOW,
        // Size and position
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        (r.right  - r.left),
        (r.bottom - r.top ),
        nullptr,
        nullptr,
        instance,
        nullptr
    );

    ARK_VERIFY(_window_handle != 0, "Could not create window handle - Aborting...");

    // Create the screen buffer.
    CreateScreenBuffer(options.buffer_width, options.buffer_height, options.buffer_bpp);
}

//------------------------------------------------------------------------------
void
Win32_Window::FlushBufferInternal(HDC const hdc)
{
    Vec2_i32 const screen_size = GetClientSize();
    Vec2_i32 const buffer_size(_screen_buffer->width, _screen_buffer->height);

    StretchDIBits(
        hdc,
        0, 0, screen_size.x, screen_size.y,
        0, 0, buffer_size.x, buffer_size.y,
        _screen_buffer->memory,
        &_screen_buffer->bitmap_info,
        DIB_RGB_COLORS, SRCCOPY
    );
}

//------------------------------------------------------------------------------
void
Win32_Window::DestroyInternal()
{
    ark::Win32::ReleaseMemory(_screen_buffer->memory);
    ark::Win32::ReleaseMemory(_screen_buffer);

    ::DestroyWindow(_window_handle) ;
}

//
// X11 Event Handlers
//
//------------------------------------------------------------------------------
void
Win32_Window::OnWindowCreate()
{
    if(!_curr_event_weak_ptr) {
        return;
    }

    _curr_event_weak_ptr->type = ark::Gfx::WindowEvent::Type::Create;
    ark::Gfx::WindowEvent::Create &data = _curr_event_weak_ptr->create_data;

    auto const &size = GetClientSize();
    data.width  = size.x;
    data.height = size.y;
}

//------------------------------------------------------------------------------
void
Win32_Window::OnWindowPaint()
{

    PAINTSTRUCT paint_struct = {};
    HDC   const hdc          = BeginPaint(_window_handle, &paint_struct);
    FlushBufferInternal(hdc);
    EndPaint(_window_handle, &paint_struct);
}


//------------------------------------------------------------------------------
void
Win32_Window::OnWindowResize()
{
    // Setup the WindowEvent object if we have one...
    if(!_curr_event_weak_ptr) {
        return;
    }

    _curr_event_weak_ptr->type = ark::Gfx::WindowEvent::Type::Resize;
    ark::Gfx::WindowEvent::Resize &data = _curr_event_weak_ptr->resize_data;

    auto const &size = GetClientSize();
    data.new_width  = size.x;
    data.new_height = size.y;
}

//------------------------------------------------------------------------------
void
Win32_Window::OnWindowClose()
{
    _wants_to_close = true;
    PostQuitMessage(0);
}

//------------------------------------------------------------------------------
void
Win32_Window::OnWindowDestroy()
{
    DestroyInternal();
}
} // namespace Gfx
} // namspace ark

#endif // ARK_OS_IS_WINDOWS
