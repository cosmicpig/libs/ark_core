#pragma once

#include "ark_core/Platform/Discovery.hpp"
#if ARK_OS_IS_GNU_LINUX
// Arkadia
#include "ark_core/Platform/PlatformIncludes.hpp"
#include "ark_core/Gfx/GfxIncludes.hpp"
#include "ark_core/Gfx/Window.hpp"
#include "ark_core/Gfx/WindowEvents.hpp"
#include "ark_core/Gfx/OffscreenBuffer.hpp"


namespace ark { namespace Gfx {

class X11_Window :
    Window
{
    friend class Window;

    //
    // CTOR / STOR
    //
public:
    ~X11_Window() = default;

    //
    // Window Interface
    //
public:
    void            * GetNativeHandle     () const override;
    u32               GetWidth            () const override;
    u32               GetHeight           () const override;
    Rect_i32          GetClientRect       () const override;
    Vec2_i32          GetClientSize       () const override;
    String            GetTitle            () const override;
    bool              WantsToClose        () const override;
    Vec2_i32          GetMousePosition    () const override;

    void SetTitle(String const &str)                    override;
    void SetSize (u32    const width, u32 const height) override;

    void Show() override;
    void Hide() override;

    bool HandleEvents        (WindowEvent *event, bool const dispatch_underlying_system_event = false) override;
    bool HandleEventsBlocking(WindowEvent *event, bool const dispatch_underlying_system_event = false) override;

    void CreateScreenBuffer(
        u32 const width,
        u32 const height,
        u32 const bpp
    ) override;

    OffscreenBuffer* GetCurrentScreenBuffer() override;
    void             FlushBuffers          () override;


    //
    // Helper Methods
    //
private:
    void InitWithOptions(Window::CreateOptions const &options);

    //
    // X11 Event Handlers
    //
private:
    void OnWindowCreate();
    void OnWindowPaint ();
    void OnWindowResize();

    //
    // iVars
    //
private:
    // X11 Things
    ::Display   *_display       = nullptr;
    ::Window     _window        = 0;
    i32          _screen        = ark::Max<i32>();
    Picture     _render_picture = ark::Max<Picture>();

    void            *_window_handle = nullptr;
    OffscreenBuffer *_screen_buf = nullptr;

    WindowEvent      *_curr_event_weak_ptr = nullptr;
    bool             _wants_to_close = false;
}; // class X11_Window


} // namespace Gfx
} // namespace ark
#endif // ARK_OS_IS_GNU_LINUX
