﻿#include "ark_core/Platform/Discovery.hpp"
#if ARK_OS_IS_WINDOWS
// Header
#include "ark_core/Gfx/GfxUtils.hpp"
// Arkadia
#include "ark_core/Platform.hpp"

//------------------------------------------------------------------------------
ark::Vec2_i32
ark::Gfx::GetDesktopSize()
{
    i32 const width  = GetSystemMetrics(SM_CXSCREEN);
    i32 const height = GetSystemMetrics(SM_CYSCREEN);

    return ark::Vec2_i32(width, height);
}

#endif // if ARK_OS_IS_WINDOWS
