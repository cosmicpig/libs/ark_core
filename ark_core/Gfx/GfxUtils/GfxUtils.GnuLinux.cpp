﻿#include "ark_core/Platform/Discovery.hpp"
#if ARK_OS_IS_GNU_LINUX
// Header
#include "ark_core/Gfx/GfxUtils.hpp"
// Arkadia
#include "ark_core/Platform.hpp"
#include "ark_core/Gfx/GfxIncludes.hpp"


//------------------------------------------------------------------------------
ark::Vec2_i32
ark::Gfx::GetDesktopSize()
{
    Display * d = XOpenDisplay(nullptr);
    Screen  * s = DefaultScreenOfDisplay(d);
    ark::Vec2_i32 ret(s->width, s->height);
    XCloseDisplay(d);

    return ret;
}

#endif // if ARK_OS_IS_GNU_LINUX
