﻿#pragma once

// Arkadia
#include "ark_core/CodeUtils.hpp"

namespace ark {



//------------------------------------------------------------------------------
// @todo(stdmatt): Do something with templates to ensure that's pod.... Jan 29, 21
template <typename Type>
ARK_FORCE_INLINE void
ResetMemory(Type *mem)
{
    ::memset(mem, 0, sizeof(Type));
}

//------------------------------------------------------------------------------
template <typename Type>
ARK_FORCE_INLINE void
ResetMemory(Type *mem, size_t const count)
{
    ::memset(mem, 0, sizeof(Type) * count);
}

//------------------------------------------------------------------------------
template <typename Type>
ARK_FORCE_INLINE void
ResetMemory(void *mem, size_t const count)
{
    ::memset(mem, 0, sizeof(Type) * count);
}

//------------------------------------------------------------------------------
template <typename Type>
ARK_FORCE_INLINE void
SetMemory(Type *mem, size_t const count, i32 const value)
{
    ::memset(mem, value, sizeof(Type) * count);
}

//------------------------------------------------------------------------------
template <typename Type>
ARK_FORCE_INLINE void
SetMemory(void *mem, size_t const count, i32 const value)
{
    ::memset(mem, value, sizeof(Type) * count);
}

//------------------------------------------------------------------------------
template <typename Return_Type>
ARK_FORCE_INLINE Return_Type*
AllocMemory(size_t const size_in_bytes)
{
    void *memory = ::malloc(size_in_bytes);
    return Cast<Return_Type*>(memory);
}

//------------------------------------------------------------------------------
ARK_FORCE_INLINE void*
AllocMemory(size_t const size_in_bytes)
{
    void *memory = ::malloc(size_in_bytes);
    return memory;
}

//------------------------------------------------------------------------------
template <typename T>
ARK_FORCE_INLINE void
FreeMemory(T *&memory)
{
    ::free(memory);
    memory = nullptr;
}

//------------------------------------------------------------------------------
ARK_FORCE_INLINE void
CopyMemory(
    void       *       dst,
    size_t       const offset,
    void const * const src,
    size_t       const src_size)
{
    ::memcpy((Cast<u8*>(dst) + offset), src, src_size);
}

} // namespace ark
