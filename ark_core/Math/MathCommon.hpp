﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"

namespace ark {

//------------------------------------------------------------------------------
template <typename T, typename U>
ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
auto Area(T const t, U const u) -> decltype(t * u)
{
    return t * u;
}

//------------------------------------------------------------------------------
// @todo(stdmatt): If is numeric is best to not pass by reference... Jan 02, 2021
template<typename Type>
ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
const Type&
Clamp(Type const &_value, Type const &_min, Type const &_max)
{
    return _value < _min ? _min :
           _value > _max ? _max :
           _value;
}

//------------------------------------------------------------------------------
// @todo(stdmatt): If is numeric is best to not pass by reference... Jan 02, 2021
template<typename Type>
ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
const Type&
Min(Type const &a, Type const &b)
{
    return (a < b) ? a : b;
}

template<typename Type>
ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
const Type&
Max(Type const &a, Type const &b)
{
    return (a > b) ? a : b;
}
} // namespace ark
