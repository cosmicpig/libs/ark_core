﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"

namespace ark {

template <typename T>
struct Vec2
{
    T x, y;

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    explicit Vec2(T const x = 0, T const y = 0)
        : x(x)
        , y(y)
    {
        // Empty...
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    static Vec2<T>
    Empty()
    {
        // @todo(stdmatt): Research if we make static and return const ref
        // if the compiler will generate better assembly and if it will be faster!!??
        return Vec2<T>();
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    bool IsEmpty() { return *this == Empty(); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    Vec2 operator*(f32 value) const
    {
        Vec2 v(x * value, y * value);
        return v;
    }

}; // struct Vec


//
// Typedefs
//
//------------------------------------------------------------------------------
typedef Vec2<i32> Vec2_i32;

} // namespace ark


// ARK_FMT_DEFINE_SIMPLE_FORMATTER(ark::VecI32, "Vec:({}, {}, {}, {}");
