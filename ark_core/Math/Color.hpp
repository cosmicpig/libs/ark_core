﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"

namespace ark {

struct Color
{
    //
    // Public Vars
    //
public:
    u32 value;

    //
    // Enums / Constants / Typedefs
    //
public:
    //--------------------------------------------------------------------------
    static Color const Red;
    static Color const Green;
    static Color const Blue;

    static Color const Cyan;
    static Color const Magenta;
    static Color const Yellow;

    static Color const Black;
    static Color const White;

    //
    // Static Methods
    //
public:
    // @todo(stdmatt): The offset of the components inside of the color is just for the
    // win32 aaRRGGBB color... We should find a way to make it customizable - Jan 24, 2021
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR static u32 ToRedU32  (u8 const r) { return Cast<u32>(r) << 16; }
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR static u32 ToGreenU32(u8 const g) { return Cast<u32>(g) <<  8; }
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR static u32 ToBlueU32 (u8 const b) { return Cast<u32>(b) <<  0 ; }
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR static u32 ToAlphaU32(u8 const a) { return Cast<u32>(a) << 24; }

    //
    // CTOR / DTOR
    //
public:
    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    Color(u8 const r, u8 const g, u8 const b, u8 const a = 0xFF)
        : value(
            Color::ToRedU32  (r) |
            Color::ToGreenU32(g) |
            Color::ToBlueU32 (b) |
            Color::ToAlphaU32(a))
    {
        // Empty...
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    Color(u32 const value = 0)
        : value(value)
    {
        // Empty...
    }
}; // struct Color


} // namespace ark
