﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"

namespace ark {

// @todo(stdmatt): Add new methods
//      - Intersects
//      - Contains
//      - Center
//      - Expand
// Jan 17, 21
template <typename T>
struct Rect
{
    T x, y, w, h;

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    explicit Rect(T const x = 0, T const y = 0, T const w = 0, T const h = 0)
        : x(x)
        , y(y)
        , w(w)
        , h(h)
    {
        // Empty...
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    static Rect <T>
    Empty()
    {
        // @todo(stdmatt): Research if we make static and return const ref
        // if the compiler will generate better assembly and if it will be faster!!??
        return Rect<T>();
    }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    bool IsEmpty() { return *this == Empty(); }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    T Left() { return x; }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    T Top() { return y; }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    T Right() { return x + w; }

    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    T Bottom() { return y + h; }

    //--------------------------------------------------------------------------
    ARK_FORCE_INLINE ARK_STRICT_CONSTEXPR
    T Area() { return w * h; }

}; // struct Rect

//
// Typedefs
//
typedef Rect<i32> Rect_i32;

} // namespace ark


// ARK_FMT_DEFINE_SIMPLE_FORMATTER(ark::Rect_i32, "Rect:({}, {}, {}, {}");
