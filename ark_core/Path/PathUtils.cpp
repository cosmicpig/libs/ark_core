﻿#include "ark_core/Platform/Discovery.hpp"
// Header
#include "ark_core/Path/PathUtils.hpp"
// Arkadia
#include "ark_core/IO.hpp"
#include "ark_core/Math.hpp"
#include "ark_core/Platform.hpp"

namespace ark {

//
// Helper Functions
//
//------------------------------------------------------------------------------
ark_internal_function bool
_Stat(String const &path, u32 const mask) {
    struct stat sb = {};
    if(stat(path.CStr(), &sb) != 0) {
        return false;
    }
    return (sb.st_mode & S_IFMT) == mask;
}

//------------------------------------------------------------------------------
bool
PathUtils::IsFile(String const &path)
{
    return _Stat(path, S_IFREG);
}

//------------------------------------------------------------------------------
bool
PathUtils::IsDir(String const &path)
{
    // @notice(stdmatt): On windows the root path is split with the drive name followed
    // by the colon, but the stat function to get it right needs the whole thing to
    // be suffixed with the slash - So we make a hack for it - April 02, 2021
#if ARK_CURRENT_OS == ARK_OS_WINDOWS
    if(path.Length() == 2 && isalpha(path[0]) && path[1] == ':'){
         return _Stat(path + ark::PATH_SEPARATOR_STR, S_IFDIR);
    }
#endif
    return _Stat(path, S_IFDIR);
}

//------------------------------------------------------------------------------
String
PathUtils::MakeRelative(
    String const &path,
    String const &to_what_path)
{
    if(path.IsEmpty() || to_what_path.IsEmpty()) {
        return "";
    }

    // @todo(stdmatt): Creating too many copies... Dec 21, 2020
    String const canonized_path    = Canonize(path        );
    String const canonized_to_what = Canonize(to_what_path);
    Array<String> path_split    = canonized_path   .Split(ark::PATH_SEPARATOR_CHAR);
    Array<String> to_what_split = canonized_to_what.Split(ark::PATH_SEPARATOR_CHAR);

    if(path_split[0] != to_what_split[0]) { // Different drives?
        return canonized_path; // Can't go there relatively...
    }

    size_t right_most_index = 0;
    for(
        size_t i = 0,
        least_count = ark::Min(path_split.Count(),  to_what_split.Count());
        i < least_count;
        ++i)
    {
        if(path_split[i] != to_what_split[i]) {
            break;
        }
        right_most_index = i;
    }

    size_t const diff           = to_what_split.Count() - right_most_index;
    String const parent_dir_str = String::Format(
        "{}{}",
        ark::PATH_PARENT_DIRECTORY_STR,
        ark::PATH_SEPARATOR_STR
    );

    String const up_to_common_parent_str = String::Repeat(parent_dir_str, diff);
    String const to_target_path_str      = String::Join(
        to_what_split.begin() + right_most_index,
        to_what_split.end  (),
        ark::PATH_SEPARATOR_CHAR
    );

    String relative_path = String::CreateWithLength(
        up_to_common_parent_str.Length() + to_target_path_str.Length()
    );

    size_t offset = 0;
    relative_path.MemCopy(offset, up_to_common_parent_str.CStr(), up_to_common_parent_str.Length());

    offset += up_to_common_parent_str.Length();
    relative_path.MemCopy(offset, to_target_path_str.CStr(), to_target_path_str.Length());

    // @todo(stdmatt): Doing super adhoc now, but we should look how .net does... Dec 21, 2020
    return relative_path;
}

//------------------------------------------------------------------------------
String
PathUtils::Canonize(
    String               const &path,
    CanonizeCaseOptions  const case_options  /* = CanonizeCaseOptions::DoNotChange  */,
    CanonizeSlashOptions const slash_options /* = CanonizeSlashOptions::ChangeToForwardSlashes */)
{
    String const abs_path = MakeAbsolute(path);
    Array<String> components = abs_path.Split({
        ark::PATH_SEPARATOR_CHAR,
        ark::PATH_ALTERNATE_SEPARATOR_CHAR
    });

    String final_path = String::CreateWithCapacity(abs_path.Length());
    for(String component : components) {
        if(case_options == CanonizeCaseOptions::ChangeToLowerCase) {
            component.ToLower();
        } else if(case_options == CanonizeCaseOptions::ChangeToUpperCase) {
            component.ToUpper();
        }
        char const slash_char =
            slash_options == CanonizeSlashOptions::ChangeToBackwardSlashes ? '\\' :
            slash_options == CanonizeSlashOptions::ChangeToForwardSlashes  ? '/'  :
            abs_path[final_path.Length()];

        final_path.Append(component);
        final_path.Append(slash_char);
    }
    // @todo(stdmatt): Super slopply way to ensure that we don't have the trailing
    // slash char on the path, find a better way to do it - Jan 02, 2021.
    if(final_path.Back() == ark::PATH_SEPARATOR_CHAR) {
        final_path.PopBack();
    }
    return final_path;
}

//------------------------------------------------------------------------------
String
PathUtils::GetTempDir()
{
    // @todo(stdmatt): implement... feb 10, 21
    return GetCurrentDir();
}

//------------------------------------------------------------------------------
time_t
PathUtils::GetCreationTimeUtc(String const &path)
{
    struct stat sb = {};
    if(stat(path.CStr(), &sb) != 0) {
        return INVALID_TIME;
    }
    return sb.st_ctime;
}

//------------------------------------------------------------------------------
time_t
PathUtils::GetAccessTimeUtc(String const &path)
{
    struct stat sb = {};
    if(stat(path.CStr(), &sb) != 0) {
        return INVALID_TIME;
    }
    return sb.st_atime;
}

//------------------------------------------------------------------------------
time_t
PathUtils::GetModifiedTimeUtc(String const &path)
{
    struct stat sb = {};
    if(stat(path.CStr(), &sb) != 0) {
        return INVALID_TIME;
    }
    return sb.st_mtime;

}

//------------------------------------------------------------------------------
size_t
PathUtils::GetFileSize(String const &path)
{
    struct stat st;
    stat(path.CStr(), &st);
    return st.st_size;
}

//
//
//
//------------------------------------------------------------------------------
String
PathUtils::Dirname(String const &path)
{
    // c:/some/nice/dir  -> c:/some/nice
    // c:/some/nice/dir/ -> c:/some/nice/dir

    // @todo(stdmatt): Handle the ark::PathAlternateChar - Dec 21, 2020
    size_t const last_index = path.FindLastIndexOf(ark::PATH_SEPARATOR_CHAR);
    String const dir_path   = path.SubString(0, last_index);
    return dir_path;
}

//------------------------------------------------------------------------------
String
PathUtils::Basename(String const &path)
{
    // c:/some/nice/dir  -> c:/some/nice
    // c:/some/nice/dir/ -> c:/some/nice/dir

    // @todo(stdmatt): Handle the ark::PathAlternateChar - Dec 21, 2020
    String const clean_path = Canonize(path);
    size_t const last_index = clean_path.FindLastIndexOf(ark::PATH_SEPARATOR_CHAR);
    String const basename   = clean_path.SubString(last_index + 1, clean_path.Length());

    return basename;
}
//
// Create Dir
//
//------------------------------------------------------------------------------
PathUtils::CreateDirResult_t
PathUtils::CreateDir(
    String           const &path,
    CreateDirOptions const options)
{
    //
    // Non Recursive
    if(options == CreateDirOptions::NonRecursive) {
        i32 const result = ark::mkdir(path.CStr());
        // Success
        if(result == 0) {
            return CreateDirResult_t::Success(true);
        }
        // Error - Known...
        if(result == -1) {
            ark::ErrorCodes::Codes code = ark::Platform::TranslatePlatformErrorToArk(errno);
            return CreateDirResult_t::Fail({ code, path });
        }

        // Error - Unkown
        return CreateDirResult_t::Fail({ ErrorCodes::UNKNOWN_ERROR, path});
    }

    // Recursive
    String const  canonized_path  = PathUtils::Canonize(path);
    Array<String> path_components = canonized_path.Split(ark::PATH_SEPARATOR_CHAR);

    String curr_path;
    for(String const &component : path_components) {
        curr_path = PathUtils::Join(curr_path, component);
        bool const is_dir = PathUtils::IsDir(curr_path);
        if(!is_dir) {
            CreateDirResult_t const result = PathUtils::CreateDir(curr_path, CreateDirOptions::NonRecursive);
            if(result.HasFailed()) {
                return result;
            }
        }
    }

    return CreateDirResult_t::Success(true);
}

//
// Create File
//
//------------------------------------------------------------------------------
PathUtils::CreateFileResult_t
PathUtils::CreateFile(
    String            const &filename,
    CreateFileOptions const options)
{
    bool const file_exists = PathUtils::IsFile(filename);
    if(file_exists && options == CreateFileOptions::ErrorIfExists) {
        return CreateFileResult_t::Fail({ ErrorCodes::FILEPATH_ALREADY_EXISTS, filename });
    } else if(file_exists && options == CreateFileOptions::IgnoreIfExists) {
        return CreateFileResult_t::Success(true);
    }

    // @todo(stdmatt): Check if the filename is a valid filename - Jan 30, 2021..
    ark::String open_mode = "a+";
    if(options == CreateFileOptions::ForceCreation) {
        open_mode = "w";
    }

    ark::File file(filename, open_mode);
    ark::Result<bool> const result = file.Open();
    if(!result.HasFailed()) {
        return result;
    }
    return CreateFileResult_t::Success(true);
}

//------------------------------------------------------------------------------
PathUtils::DeleteDirResult_t
PathUtils::DeleteDir(String const &path)
{
    bool const result = ::rmdir(path.CStr());
    if(result != 0) {
        ark::Platform::TranslateCLibErrorToArk(result);
    }
    return DeleteDirResult_t::Success(true);
}

//------------------------------------------------------------------------------
PathUtils::DeleteFileResult_t
PathUtils::DeleteFile(String const &filename)
{
    bool const result = ::unlink(filename.CStr());
    if(result != 0) {
        ark::Platform::TranslateCLibErrorToArk(result);
    }
    return DeleteDirResult_t::Success(true);
}

} // namespace ark
