﻿#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Debug.hpp"
#include "ark_core/Platform/PlatformIncludes.hpp"
#include "ark_core/Containers/String.hpp"


namespace ark {

class DateTime
{
public:
    enum class Format { Short, Long };


public:
    ARK_FORCE_INLINE static
    DateTime Now()
    {
        return DateTime(time(nullptr));
    }

public:
    explicit
    DateTime(ark::time_t const timestamp)
    {
        // @todo(stdmatt): How reliable is this function? Will it fail? - Feb 8, 2021
        ark::tm_t *result = ark::gmtime(&timestamp);
        ARK_ASSERT(result != nullptr, "Failed to gmtime with timestamp == ({})", timestamp);
        _tm = *result;
    }

public:
    ARK_FORCE_INLINE u32
    Timestamp() const
    {
        return Cast<u32>(mktime(Cast<ark::tm_t*>(&_tm)));
    }


    ARK_FORCE_INLINE u32 Year   () const { return _tm.tm_year + 1900; }
    ARK_FORCE_INLINE u32 Month  () const { return _tm.tm_mon  +    1; }
    ARK_FORCE_INLINE u32 Day    () const { return _tm.tm_mday;        }
    ARK_FORCE_INLINE u32 Hours  () const { return _tm.tm_hour;        }
    ARK_FORCE_INLINE u32 Minutes() const { return _tm.tm_min;         }
    ARK_FORCE_INLINE u32 Seconds() const { return _tm.tm_sec;         }

public:
    String YearAsString   () const { return String(Year   ()); }
    String DayAsString    () const { return String(Day    ()); }
    String HoursAsString  () const { return String(Hours  ()); }
    String MinutesAsString() const { return String(Minutes()); }
    String SecondsAsString() const { return String(Seconds()); }

    String MonthAsString(Format const format = Format::Short) const
    {
        // @TODO(stdmatt): Improve this... Feb 05, 21
        String __months[] = {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        };

        ark::String month_name = __months[Month() -1];
        if(format == Format::Long) {
            return month_name;
        }
        return month_name.SubString(0, 3);
    }

public:
    ark::String
    ToDebugString() const
    {
        return ark::String::Format(
            "{} {}, {} - {}:{}:{}",
            MonthAsString(), Day(), Year(),
            Hours(), Minutes(), Seconds()
        );
    }


private:
    ark::tm_t _tm;
}; // struct DateTime

} // namespace ark
