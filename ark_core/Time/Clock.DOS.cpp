// Arkadia
#include "ark_core/Platform/Discovery.hpp"
#if defined(ARK_OS_IS_DOS)
// Header
#include "ark_core/Time/Clock.hpp"
#include "ark_core/Platform.hpp"

namespace ark {

//
// Globals
//
ark_global_var bool g_TicksStarted = false;


//
// Helper Functions
//
//------------------------------------------------------------------------------
ark_internal_function void
_ark_TicksInit()
{

    // @todo(stdmatt): Implement.... Jan 30, 21
    if(g_TicksStarted) {
        return;
    }
}

//
// Static Functions
//
//------------------------------------------------------------------------------
u32
Clock::GetTicks()
{
    if (!g_TicksStarted) {
        _ark_TicksInit();
    }

    // @todo(stdmatt): Implement.... Jan 30, 21
    u32 ticks = 0;
    return ticks;
}

//------------------------------------------------------------------------------
f32
Clock::DivisorOfTicksPerSeconds()
{
    // @todo(stdmatt): Implement.... Jan 30, 21
    return 1000.0f;
}

} // namespace ark
#endif // defined(ARK_OS_IS_DOS)
