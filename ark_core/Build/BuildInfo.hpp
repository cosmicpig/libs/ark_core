#pragma once
// Arkadia
#include "ark_core/CodeUtils.hpp"
#include "ark_core/Containers/String.hpp"
#include "ark_core/Time/DateTime.hpp"

namespace ark { namespace Build {

struct BuildInfo {
    char const * const ProgramName;
    u32          const  VersionMajor;
    u32          const  VersionMinor;
    u32          const  VersionBaby;
    u32          const  CopyrightYearStart;
    u32          const  CopyrightYearEnd;

    BuildInfo(
        char const * const _ProgramName,
        u32          const  _VersionMajor,
        u32          const  _VersionMinor,
        u32          const  _VersionBaby,
        u32          const  _CopyrightYearStart,
        u32          const  _CopyrightYearEnd = DateTime::Now().Year()
    )
        : ProgramName       (_ProgramName        )
        , VersionMajor      (_VersionMajor       )
        , VersionMinor      (_VersionMinor       )
        , VersionBaby       (_VersionBaby        )
        , CopyrightYearStart(_CopyrightYearStart )
        , CopyrightYearEnd  (_CopyrightYearEnd   )
    {
        // Empty...
    }

    //------------------------------------------------------------------------------
    static ARK_FORCE_INLINE ark::String
    BuildVersionString(
        u32         const major,
        u32         const minor,
        u32         const baby,
        ark::String const &prefix = "",
        ark::String const &suffix = "")
    {
        return ark::String::Format("{}{}.{}.{}{}", prefix, major, minor, baby, suffix);
    }

    //------------------------------------------------------------------------------
    static ARK_STRICT_CONSTEXPR u32 BUILD_COPYRIGHT_STRING_INVALID_YEAR = ark::Max<u32>();

    static ARK_INLINE ark::String
    BuildCopyrightString(
        u32         const begin_year,
        u32               end_year = BUILD_COPYRIGHT_STRING_INVALID_YEAR)
    {
        if(end_year == BUILD_COPYRIGHT_STRING_INVALID_YEAR) {
            end_year = ark::DateTime::Now().Year();
        }
        u32 const diff = (end_year - begin_year);
        if(diff == 0) {
            return ark::String(end_year);
        } else if(diff == 1) {
            return ark::String::Format("{}, {}", begin_year, end_year);
        } else {
            return ark::String::Format("{} - {}", begin_year, end_year);
        }
    }

    ARK_FORCE_INLINE ark::String BuildSimpleVersionString() const
    {
        return BuildInfo::BuildVersionString(VersionMajor, VersionMinor, VersionBaby);
    }

    ARK_FORCE_INLINE ark::String BuildSimpleCopyrightString() const
    {
        return BuildInfo::BuildCopyrightString(CopyrightYearStart, CopyrightYearEnd);
    }
};

#define _ARK_BUILD_INFO_MAKE_VAR_NAME(n) BuildInfo_##n
#define ARK_BUILD_INFO_MAKE_VAR_NAME(n) _ARK_BUILD_INFO_MAKE_VAR_NAME(n)
#define ARK_PROGRAM_SET_BUILD_INFO(_major_, _minor_, _baby_, _name_, _copyright_year_) \
    static ark::Build::BuildInfo ARK_BUILD_INFO_MAKE_VAR_NAME(_name_)(ARK_STRINGIZE(_name_), _major_, _minor_, _baby_, _copyright_year_)


//
// Assertions
//
ARK_STRICT_CONSTEXPR ARK_FORCE_INLINE bool IsAssertEnabled() { return ARK_ASSERTION_IS_ENABLED; }
ARK_STRICT_CONSTEXPR ARK_FORCE_INLINE bool IsDebugBuild   () { return ARK_IS_DEBUG_BUILD;       }

//
// Functions
//
//------------------------------------------------------------------------------



} // namespace BuildInfo
} // namespace ark
